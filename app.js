const express = require('express');
const moment = require('moment');
const session = require('express-session');

moment.locale('ru');

app = express();

app.use(express.static('./frontend/public'));
app.set('view engine', 'pug');
app.locals.moment = moment;

app.use('/', require('./frontend'));

app.use((req, res) => {
    res.status(404).render('../frontend/pages/404');
});

app.listen(3000, () => {
    console.log('listen 3000');
});