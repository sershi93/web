$(document).ready(function () {
	$(document).on('click', '.task-table tr', function () {
		$(this).find('.js-modal-init').trigger('click');
	});

	$(".search input").on("change paste keyup", function() {
		var value = $(this).val().trim();
		if (value.length === 0) {
			$('.task-table tr.hidden').each(function () {
				$(this).removeClass('hidden');
			});
		} else {
			$('.task-table tr').each(function () {
				var title = $(this).children('td').eq(1).text();
				$(this).removeClass('hidden');
				if (title.length > 0 && title.indexOf(value) === -1) {
					$(this).addClass('hidden');
				}
			});
		}
	});
});