$(document).ready(function () {
	$(document).on('click', '.js-modal-init', function (e) {
		e.preventDefault();
		$("#task-modal").arcticmodal();

		var id = $(this).text();

		$.get('/task/'+id, function (response, status) {
			$('.box-modal__title').html('Информация и задаче №' + response.id);
			$('#task-title').html(response.title);
			$('#task-date').html(response.date);
			$('#task-author').html(response.author);
			$('#task-status').html(response.status);
			$('#task-description').html(response.description);
		});
		// 	var params = response;
		// 	$('#catalog-modal .modal-catalog .box-modal__title')[0].innerHTML = params.name_full;
		//
		// 	$('#catalog-modal .modal-catalog .modal-catalog__head .modal-catalog__img')[0].innerHTML = '';
		// 	$('#catalog-modal .modal-catalog .modal-catalog__info')[0].innerHTML = '<li>Актуальность: <b>' + params.actual + '</b></li>' +
		// 		'<li>Дата публикации: <b>' + params.release + '</b></li>';
		// 	$('#catalog-modal .modal-catalog .modal-catalog__buttons')[0].innerHTML = '<a class="button button--red" href="https://212709.selcdn.ru/autodealer-site/public/downloads_file/soft/autodealer/rtimes/' + params.filename + '">Загрузить ' + params.size + ' мб</a>' +
		// 		'<a class="button button--blue-border" href="/faq">Как установить?</a>';
		//
		// 	$('#catalog-modal .modal-catalog .modal-catalog__demo')[0].innerHTML = '<div class="modal-catalog__demo-pin">Если у вас нет ПО Автодилер, скачайте демо версию для полноценного использования базы каталогов</div>' +
		// 		'<div class="modal-catalog__demo-button"><a class="button button--blue-border" href="/demo">демоверсия</a></div>';
		// });
		return false;
	});
});