const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const request = require('request');
const cache = require('memory-cache');
const moment = require('moment');

const fs = require('fs');

router.use(bodyParser.urlencoded({extended: true}));


router.get('/task/:id', (req, res) => {
	let id = req.params.id;
	if (!cache.get('task' + id)) {
	    request.get('http://127.0.0.1:8000/api/v1/task/' + id, (error, response, body) => {
		    body = JSON.parse(body);
		    body.date = moment(body.date).format('hh:mm:ss DD.MM.YYYY');
		    cache.put('task' + id, body);
		    return res.json(body);
	    });
	} else {
		return res.json(cache.get('task' + id));
	}
});

router.use('/', (req, res) => {
	request.get('http://127.0.0.1:8000/api/v1/task', (error, response, body) => {
		let tasks = JSON.parse(body);
		res.render('../frontend/pages/home', {tasks, moment});
	});
});

module.exports = router;