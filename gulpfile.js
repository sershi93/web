'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const csso = require('gulp-csso');
const sourcemaps = require('gulp-sourcemaps');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');

const pump = require('pump');


// const cheerio = require('gulp-cheerio');
// const replace = require('gulp-replace');

gulp.task('sass', function(){
    gulp.src('./frontend/styles/style.scss')
    	.pipe(sourcemaps.init({debug: false, identityMap: true}))
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 5 versions'],
            cascade: false
        }))
        .pipe(csso())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./frontend/public/build'));
});

gulp.task('js', function(cb) {
    let stream = [gulp.src([
        './frontend/scripts/**/*.js',
        './frontend/blocks/**/*.js'
    ])];
    if (process.env.NODE_ENV === 'production') {
        //stream.push(uglify());
    }
    stream.push(concat('app.js'));
    stream.push(gulp.dest('./frontend/public/build'));
    pump(stream, cb);
});